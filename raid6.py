#!/usr/bin/env python


# shelf configuration
drives = 23             # scalar
throughput = 10         # sustained rebuild throughput in MB/s

# Specs from Seagate Cheetah 15k.7:
# http://www.seagate.com/files/docs/pdf/datasheet/disc/cheetah-15k.7-ds1677.3-1007us.pdf
drivecapacity = 600e3   # MB
mtbf = 365 * 5          # days (actual Cheetah 15k MTBF is 1.6e6 hours)
ure = 1e16              # Unrecoverable Read Error (bit)rate

# calculate MTTR in days:
mttr = (drivecapacity / float(throughput)) / float((3600 * 24))

# calculate MTTDL in days when 3 drives fail:
# http://www.servethehome.com/raid-reliability-failure-anthology-part-1-primer/
mttdl3 = mtbf**3 / (drives * (drives - 1) * (drives - 2) * mttr**2)
drivefailure3 = 1 / (mttdl3 / 365.0)

# calculate MTTDL in days when 2 drives fail and there is an URE:
mttdl2 = mtbf**2 / (drives * (drives - 1) * mttr)
drivefailure2 = 1 / (mttdl2 / 365.0)
rebuildfailure = (drivecapacity * (drives - 2)) / (ure / float((1024 * 1024 * 8)))
drivefailureure = ((drivefailure2 * rebuildfailure) * 100.0)

report = """\
Probability of a RAID 6 (double parity) failure, leading to total loss of all
data.

Array/shelf configuration:
Drives:         %(drivecount)d (2 parity drives)
Total capacity: %(shelfcapacity)dTB
MTTR:           %(mttr)d hours (rebuild time)
Hot spares:     infinite

Drive parameters:
Capacity:       %(drivecapacity)dGB
MTBF:           %(mtbf)d hours
URE rate:       %(ure).e

Annual probability that 3 drives fail within the MTTR window (thereby
destroying all data):

    %(drivefailure3).3f%%

Annual probability that 2 drives fail (%(drivefailure2).3f%%), combined with an
Unrecoverable Read Error (URE) during the rebuild (%(rebuildfailure).3f%%):

    %(drivefailureure).3f%%
"""

print report % {
    'drivecount': drives,
    'shelfcapacity': (((drives - 2) * drivecapacity) / (1024 * 1024)),
    'mttr': (mttr * 24),
    'drivecapacity': (drivecapacity / 1024),
    'mtbf': (mtbf * 24),
    'ure': ure,
    'drivefailure2': drivefailure2 * 100.0,
    'drivefailure3': drivefailure3 * 100.0,
    'rebuildfailure': rebuildfailure,
    'drivefailureure': drivefailureure,
}
