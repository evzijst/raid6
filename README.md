Probability of a RAID 6 (double parity) failure, leading to total loss of all
data.

Array/shelf configuration:

	Drives:         23 (2 parity drives)
	Total capacity: 12TB
	MTTR:           16 hours (rebuild time)
	Hot spares:     infinite

Drive parameters:

	Capacity:       585GB
	MTBF:           43800 hours
	URE rate:       1e+16

Annual probability that 3 drives fail within the MTTR window (thereby
destroying all data):

    0.031%

Annual probability that 2 drives fail (3.851%), combined with an
Unrecoverable Read Error (URE) during the rebuild (0.011%):

    0.041%

